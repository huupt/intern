from .models import Cat
from .serializers import CatSerializer

from rest_framework.viewsets import ModelViewSet


from rest_framework.viewsets import ModelViewSet
class CatViewSet(ModelViewSet):
    queryset = Cat.objects.all()
    serializer_class = CatSerializer
