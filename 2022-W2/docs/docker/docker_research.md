# DOCKER
## 1. What is Docker?
Docker is an open platform for developing, shipping, and running applications. Docker enables you to seperate your applications from your infrastructure so you can deliver software quickly. \
Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allows you to run many containers simulteneously on a given host.\
**Docker Development Workflow**
- Write the code and test
- Create a DockerFile
- Create Images defined at Dockerfile
- (Opt) Define services by writing `docker-compose.yaml`
- Run Containers/Compose app

**Docker architecture**\
![Docker architecture](../../images/docker/architecture.svg)

Docker uses a client-server architecture. The Docker client talks to the Docker deamon, which does the heavy lifting of building, runnungm and distributing your Docker containers. Docker client and deamon can run on the sam system, or you can connect a Docker client to a remote Docker deamon.
- Docker deamon: manages Docker objects such as images, containers, networks, and volumes
- Docker client: the command line tool that allows the user to interact with the deamon
- Docker Hub: a registry of Docker images.


## 2. Docker Image
Docker images are read-only templates with instructions to create a docker container. Docker image can be pulled from a Docker hub and used as it is, or you can add additional instructions to the base image and create a new and modified docker image. You can create your own docker images also using a dockerfile. Create a dockerfile with all the instructions to create a container and run it; it will create your custom docker image.

Docker image has a base layer which is read-only, and the top layer can be written. When you edit a dockerfile and rebuild it, only the modified part is rebuilt in the top layer.

To see Docker Images on you system:\
`docker images` or\
`docker image ls`\
![Docker Images](../../images/docker/docker-images.png)
- Tag: identifies the image by its tag, such as version number.
- Image ID: is a unique image identity.
- Created: the period of time since it was created.
- Size: the image's virtual size.

**Docker Image Commands**
- To build a Docker Image from a Dockerfile:\
`docker image build <Dockerfile>` or\
`docker build <Dockerfile>`
- To remove a Docker Image:\
`docker image rm <...>` or\
`docker rm <...>`
- To remove unused images:\
`docker image prune`
- To pull a Docker Image or a repository from a registry:\
`docker pull <repository>` or \
`docker image pull <repository>`
- To push a Docker Image or a repository to a registry:
`docker push <repository>` or \
`docker image push <repository>`
- To see detailed information on one or more images:\
`docker image inspect <image>`

## 3. Docker Container
A Docker container is a virtualized container runtime environment that provides isolation capabilities for seperating the execution of applications from underpinning system. Each container is autonomous and runs in its own isolated environment, ensuring it does not disrupt other running applications or its underlying system.

- To create a Docker Container, run a Docker Image:\
`docker run <image>`\
With options:
    - `--detach` or `-d` : run container in background and print container ID
    - `--env` or `-e` : set environment environment
    - `--name` : assign a name to the container
    - `--net` or `--network` : connect a container to a network
    - `--publish` or `-p` : publish a container's port to the host network
    - `--volume` or `-v` : bind mount a volume
- To see Docker Containers **running** on your system:\
`docker ps`   or \
`docker container ls`
- To see Docker Containers on you system:
`docker ps -a` or\
`docker container ls -a`
- To stop a Docker Container: \
`docker container stop <container>` or \
`docker stop <container>`
- To start a Docker Container: \
`docker container start <container>` or \
`docker start <container>`
- To remove all stop Docker containers: \
`docker container prune`
- To remove a Docker Container: \
`docker rm <container>` or \
`docker container rm <container>` 
- To see detailed information on one or more containers: \
`docker container inspect <container>`
- To copy a file from a Docker Container to the host:\
`docker cp <container>:</source/file/path> </destination/file/path>`
- To copy a file from the host to a Docker Container: \
`docker cp </source/file/path> <container>:</destination/file/path>` 

**Run a command in a running container**\
`docker exec [OPTIONS] CONTAINER COMMAND [ARG...]` or \
`docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]` \
This command only runs while the container's primary process is running, and it is not restarted if the container is restarted. This command will run in the default directory of the container. If the underlying image has a custom directory specified with the `WORKDIR` in Dockerfile, this will be used instead.\
Options: \
    - `-d` : detached mode: run command in the background \
    - `-e` : set environment variable \
    - `-i` : keep STDIN open even if not attached \
    - `-t` : allocate a pseudo-TTY \
Example: 
- List information about the files in the current directory in the container \
`docker exec src_web_1 ls`
- Execute an interative bash shell on the container \
`docker exec -it src_web_1 bash` 

**Fetch the logs of a container** \
`docker logs [OPTIONS] CONTAINER` or \
`docker container logs [OPTIONS] CONTAINER` \
Options: \
    - `--details` : show extra details provided to logs \
    - `--follow` or `-f`: follow log output \
    - `--since` : show logs since timestamp or relative \
    - `--tail` or `-n`: number of lines to show from the end of the logs \
    - `--timestamps` or `-t` : show timestamps \
    - `--until` : show logs before a timestamp or relative 
## 4. Dockerfile
Dockerfile is a text document that contains all the commands a user could call on the command line to assemble image. Docker can build images automatically by reading the instructions from a Dockerfile. 
### Commands:
**FROM**\
The `FROM` instruction initailizes a new build stage and sets the Base Image for subsequent instructions. A valid Dockerfile must start with a `FROM` instruction. \
**RUN** \
The `RUN` instruction will execute any command in a new layer on top of the current image and commit the results. The resulting committed image will be used for the next step in the Dockerfile.
```
RUN <command> #Shell form
RUN ['executable', 'param1', 'param2'] #Exec form
```
**CMD**\
The `CMD` instruction has three forms:
```
CMD ['executable', 'param1', 'param2'] #Exec form
CMD ['param1', 'param2']
CMD command param1 param2 #Shell form
```
There can only be one `CMD` instruction in a Dockerfile. The main purpose of a `CMD` is to provide defaults for an executing container. \
*Note: `RUN` actually runs a command and commits the result, `CMD` does not execute anything at build time, but specifies the intended command for the image.*

**LABEL** 
```
LABEL <key>=<value>
```
The `LABEL` instruction adds metadata to an image. An image can have more than one label.\

**EXPOSE**
```
EXPOSE <port> [<port>/<protocol>...]
```
The `EXPOSE` instruction informs Docker that the container listens on the specified network ports at runtime. The Docker `EXPOSE` instrucion does not actually publish the port. It functions as a type of documentation between the person who builds the image and the person who runs the container, about the ports are intended to be published.\
To actually publish the port, example:
```
docker run -p 80:80
```
**ENV**
```
ENV <key>=<value> ...

```
The `ENV` instruction sets the environment variables `<key>` to the value `<value>`. This value will be in the environment for all subsequent instruions in the build stage and can be replaced. \
**ADD**
```
ADD [--chown=<user>:<group>] <src>... <dest>
ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]
```
The `ADD` instruction copies new files, directories or remote file URLs from `<src>` and adds them to the filesystem of the image at the path `<dest>` \
**COPY**
```
COPY [--chown=<user>:<group>] <src>... <dest>
COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]
```
The `COPY` instruction copies new files or direcrories from `<src>` and adds them to the filesystem of the container at the path `<dest>`\
**ENTRYPOINT**
```
ENTRYPOINT ["executable", "param1", "param2"]
ENTRYPOINT command param1 param2
```
An `ENTRYPOINT` allows you to configure a container that will run as an executable\
**VOLUME**
```
VOLUME ["/data"]
```
The `VOLUME` instruction creates a mount point with the specified name and parks it as holding externally mounted volumes from nativa host or other containers \
**WORKDIR**
```
WORKDIR /path/to/workdir
```
The WORKDIR instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile. If the WORKDIR doesn’t exist, it will be created even if it’s not used in any subsequent Dockerfile instruction.

## 5. Docker Compose
Docker Compose is a tool for defining and running multi-container Docker application using a YAML file to configure your application's services. With a single command, you create and start all services from your configuration. \
**Docker Compose Workflow** \
    - Define app environment with a Dockerfile \
    - Define the services in `docker-compose.yml` \
    - Run `docker-compose up`

**Example using Docker Compose** \
*Using Docker with MySQL and Django*
`docker-compose.yml` file: \
```
version: '3.3'
services:
  db:
    image: mysql:5.7
    ports:
      - 13306:3306
    environment:
      MYSQL_DATABASE: PetStore 
      MYSQL_USER: huu
      MYSQL_PASSWORD: HPhan1234@
      MYSQL_ROOT_PASSWORD: HPhan1234@
    volumes:
      - myvolume:/var/lib/mysql
  web:
    build: .
    command: bash -c "./wait-for-it db:3306 && python manage.py migrate && python manage.py runserver 0.0.0.0:8000"
    ports:
      - 8000:8000
    volumes:
      - .:/app
    depends_on:
      - db

volumes:
  myvolume:
```

`version` : define version of Docker Compose \
`services` : configure services \
`db`, `web` : name of the service \
`image` : specify the image to start the container from \
`build` : build the image from Dockerfile \
`port` : expose ports (HOST:CONTAINER) \
`environment` : add environment variables \
`volumes` : mount host paths and named volume to a service