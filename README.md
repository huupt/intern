# Submission guide
test``
- All documentation and submission must be managed by Gitlab (https://about.gitlab.com/)
- Repository structure
    + `src`: for code and scripts only
    + `docs`: for documentations only
    + `FEEDBACK.md`: only mentors can write this file. It means all feedback of mentors is in this file.
    + `README.md`: owned by trainees. They can write several important notes or instruction
    + `images`: for result or question images
    + Example:
    ```
    ├── <week-name> (E.g: 2021-W30) (Week for training)
    └── src 
          └── reverse_string.py
    └── docs
          └── gitlab.md
    ├── images
          └── 2021-09-13.baseball-game-result.png
    ├── FEEDBACK.md
    ├── README.md
    ```
