# Git

## 0. Require

- Estimate time: 1 day
- Raise any questions when problem occurs

## 1. Concept

- [Git simple guide](https://rogerdudler.github.io/git-guide/)
- [Git commit convention](https://www.conventionalcommits.org/en/v1.0.0/)
- [Git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

## 2. Exercise

- [Git research](https://drive.google.com/file/d/1VtS7i9c6mgYNv3TWNXrLhZScEBtlEKYs/view?usp=drivesdk)
- [Git practice](https://drive.google.com/file/d/1YfZ-RqY5EHZmxt7Qo-lS7BinTAgIgdWJ/view?usp=drivesdk)
