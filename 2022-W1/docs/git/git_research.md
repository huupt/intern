#RESEARCH ABOUT GIT
1. Repository: repository contains a collection of files various different versions of a Project. These files are imported from the repository into the local server of the user for further updations and modifications in the content of the file.
- Remote repository: git repository that is stored on some remote computer.
- Local repository: git repository that is stored on local (your) computer

2. Working tree is the set of all files and folders a developer can add, edit, rename and delete during application development
   Index is where you place files you want commit to the repository

3. Clone: copy an existing repository to another location (usually your local computer).
*Requirements:* an existing remote repository, permission.
*How to clone a repo*
Using command:
- Clone a local repo:
`git clone /path/to/repository`
- Clone a remote repo over SSH
`git clone ssh://git@hostname:/path/to/repository`
- Clone a remote repo over HTTPS
`git clone https://repo_url`

4. Stages of a file:
- Committed: file has added to HEAD
- Unmodified: file hasn't been modified since the last commit  - Modified: file has been modified since the last commit
- Untracked: file hasn't added to stage area 
- Unstagead: file has been added to stage area, modified but hasn't updated to stage - Staged: file is added to stage area

To check file status, using `git status`

5. Add a file to stage area:
`git add <file_name>`
To Unstage a file:
`git reset <file_name>`

6.

a.txt is modified and added to index
b.txt is modified in work tree, hasn't added to index
c.txt is modified in index and work tree
d.txt is added to index
e.txt is added to index, modified in work tree but hasn't yet add to index again.
f.txt is untracked (not yet `git add ...` and `git commit`)

7.
`git diff` : view the different you made relative to the index
`git diff --staged` : shows different between files in the index and committed

8. Commit: save all index (stage area) changes, usually along with a message (description)
To commit, using `git commit -m "Description of the commit"`

9. `git rm`
`git rm -f file.txt` : remove `file.txt` from both the repository and working directory.
`git rm --cached file.txt` : only remove `file.txt` from the index

10. Rename and Move a file.
- Rename a file:
`mv old_name new_name`
- Move a file:
`mv path/file/move dest/path`

11. Ignore in git: untracked files (such as Secret key, Database config,...)
Step 1: Create `.gitignore` file
Step 2: Edit `.gitignore` file, example you want to ignore file `config.py` and folder `env`, do like this:
```
config.py
/env
```

12. Git Log
`git log` : shows the commit logs
`git log --oneline`: shows all the commit logs (one line only)
`git show` : shows the latest commit the current branch
`gitk` : shows all the infomation of every commit with GUI

13. Git Remote
`git remote add origin <url>` : add a remote named `origin` for the repository at `<url>`
`git remote add <shortname> <url>` : add a remote named `<shortname>` for the repository at `<url>`
`git remote -v` : see the remote repo 
`git remote remove <shortname>` : remove the remote named `<shortname>`. All remote-tracking branches and config setting for the remote are removed

14. Branch: is a version of the repository, or an independent line of development.
Why using branch:
- Manage version of a project 
- Development features of a project

15. Using Branch
- List all branch: `git branch`
- Create a new branch: `git branch new_branch`
- Checkout a branch: `git checkout branch_name`
- Delete a branch: `git branch -d branch_name`
- Rename a branch: 
```
git checkout old_branch_name
git branch -m new_branch_name	
```

16. Merge: is used to combine two branches
*How to merge*
- Checkout to branch you want to merge into, example `develop` branch:
`git checkout develop`
- Merge `feature_1` branch to `develop` branch:
`git merge feature_1`

17. Conflict: occurs when 2 branches have made edits to the same line in a file or when a file has been deleted in one branch but edited in the other.
Resolve the conflict:
- Method 1: Using Merge Request
- Method 2: Using commands
`git status` : identify conflicted files
`git log --merge`: show a log with a list of commits that conflict between the merging branches
`git diff ...` : find differences between states of files

18. You should avoid working on master branch because master branch is the official working version of a project, used to deploy and need to be stable. If you make any changes to master branch while other people working on it, it may occur conflicts and bugs in product.

Git Workflow:
- Create a new branch for feature: `git branch feature_game`
- Checkout new branch and working on it: `git checkout feature_game`
- Push your work to new branch: `git push -u origin feature_game`
- When your done your work and testing, merge to master:
`
git checkout master
git merge feature_game
`
19. Difference between Fetch and Pull
`Pull`: automatically merges the commits into the branch you are currently working on
`Fetch`: gathers any commits from the target branch that do not exist in your current branch and stores them in local repository (does not merge with your current branch)

20. Push: used to upload local repository content to a remore repository

21. Merge request: request to merge in code from one branch to another
Using merge request to resolve conflicts when merge two branches.

22. Revert: revert a particular commit, or a set of commits
How to revert: `git revert <commit>`

23. Git Revert
Revert to commit_Id_2: `git revert commit_Id_2`
Revert before commit_Id_1: `git revert HEAD~5`

24. Git Reset
`git reset --hard` : reset the index and working tree to match the most recent commit
`git reset --soft` : do not touch the index file nor the working tree
`git reset --mixed` : reset the index to match the most recent commit, but leave the working tree unchanged
`git reset` : reset the index to match the most recent commit, but leave the working tree unchanged

25. Git Stash
- Save your work in `issue1` branch:
`git stash`
- Checkout to `issue2` branch:
`git checkout issue2`
- Commit your work in `issue2` branch
- Checkout to `issue1` branch:
`git checkout issue1`
- Get your work back:
`git stash pop`

26. Git Clean
`git clean -f` : remove untracked files
`git clean -f -d` : remove untracked files and folders
`git clean -n -d` : show what untracked folders would be removed

27. Git Rebase
Rebase: rewrite commits from one branch onto another branch and delete that branch

Using rebase when you want to save all commits (history changes)




