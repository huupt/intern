# Python

## 0. Python Modules and Packages
### 0. Modules
#### Introduction
Module is a file containing Python code. Module allows you to group together related functions, classes and code in general. Using modules helps us simplify code maintenance and comprehend of code, testing, using, reuse and scoping. The name of a module is the name of the file.

A module can contain:
- Function
- Classes
- Variables
- Executable code
- Attributes acssociated with the module such as its name
#### Importing modules
- Importing a Module\
`import checkin`
- Importing from a Module\
`from checkin import get_data_checkin`\
or with alias:\
`from checkin import get_data_checkin as checkin_data`
#### Module properties
`__name__` : the name of the module\
`__doc__` : the doctoring for the module\
`__file__` : the file in which the module was defined
#### Python module search path
Python will search though each of locations in turn to find the named module:
- The current directory
- If the module isn't found, Python the seaches each directory in the shell variable PYTHONPATH
- If all else fails, Python checks the default path. On linux, the defaul path is normally /use/local/lib/python/
### 1. Packages
A package is a directory containing one or more Python source files and an optional source file name `__init__.py` - this file may also contain code that is executed when a module is imported from the package. 
```
── Storage
    ├── __init__.py
    ├── Checkin.py
    ├── S3ToJetson.py
    └── JetsonToS3.py
``` 
The contents of the `__init__.py` fill will be run once, the first time either module within the package is referenced

#### Import Package
```
import Storage       #Import all modules in Storage Package
from Storage.Checkin import *       #Import all from Checkin Module
from Storage.Checkin import get_data_checkin as checkin_data #Import with alias name
```
`__all__` variable in the `__init__.py` file: control over what is imported from a package. `__all__` affects the  `from <packages> import *` only.

## 1. Decorators
### 0. Introduction
A decorator is a piece of code that is used to mark a callable object (function, method, class, object)to enhance or modify its behaviour
### 1. Defining a Decorator
To define a decorator you need to define a callable object such as a function that takes another function as a parameter and return a new funtions

Example: 
```
def logger(func):
    def wrapper():
        print('Before calling funtion.')
        func()
        print('After calling funtion.')
    return wrapper()
```
### 2. Using a Decorator
Example using a decorator:
```
def target():
    print('In target function')

t1 = logger(target)
t1()
```
In the example, the name `t1` now points to the `wrapper()` inner function. When we run this code, we actually execute the `wrapper()` function which was returned by the decorator.

Using decorator with syntactic sugar `@`
```
@logger
def target():
    print('In target function')

target()
```
This has the same effect as passing target into the `logger` fuction.
### 3. Function with parameters
Decorators can be applied to functions that take parameters, however the decorator function must also take these parameters as well.
Example
```
@logger
def target(x, y):
    print(x+y)

target(x, y)
```

```
def logger(func):
    def inner(x, y):
        print('Before calling funtion')
        func(x, y)
        print('After calling funtion')
    return inner
```
Note: You can use `*args` and `*kwargs` instead.
### 4. Stacked decorators
You can apply several decorators to a function by staking them on top of each other. When this occurs, each function is wrapped inside another function.
Example:
```
@logger
@debug
def hello():
    print('Hello World!')
```