# Python

## 0. Require

- Estimate time: 4 day
- Raise any questions when problem occurs

## 1. Concept

- Python tutorial
    + https://www.tutorialspoint.com/python/index.htm)
	+ https://www.python.org/
	+ https://www.w3schools.com/python/
- [Module and package](https://realpython.com/python-modules-packages)
- [Decorator](https://realpython.com/primer-on-python-decorators/)

## 2. Exercise

- Create a leetcode account, then do all problem listed on google sheet: [Excercise list](https://docs.google.com/spreadsheets/d/1QY6ZUgnxbyza9N2QK_6JC-NYonWdj3xz/edit?usp=sharing&ouid=107522219829362241337&rtpof=true&sd=true)
- Prepare presentation with the aim to:
	+ Showing the experiment of result
	+ Run simple **your own** python code with decorator, module & package
