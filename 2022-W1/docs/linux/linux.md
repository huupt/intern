# Linux

## 0. Require

- Estimate time: 2 days
- Raise any questions when problem occurs

## 1. Concept

- Terminal & Shell
    + [Linux Command Line tutorial](https://www.youtube.com/watch?v=YHFzr-akOas&list=PLS1QulWo1RIb9WVQGJ_vh-RQusbZgO_As)
    + [Linux Command Line Cheatsheet](https://cheatography.com/davechild/cheat-sheets/linux-command-line/)
    + [The linux command line for beginners](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview)
    + [Basic Command (Video)](https://www.youtube.com/watch?v=YHFzr-akOas&list=PLS1QulWo1RIb9WVQGJ_vh-RQusbZgO_As)
    + [Basic Command (Docs)](https://www.hostinger.com/tutorials/linux-commands)
    + [Bash, Zsh & Fish](https://zellwk.com/blog/bash-zsh-fish/)
    + [Terminal emulator](https://itsfoss.com/linux-terminal-emulators/)
    + [Install zsh on ubuntu (Can use on another debian-base distros)](https://linuxhint.com/installing_zsh_ubuntu/)
    + [Install fish on ubuntu (Can use on another debian-base distros)](https://gist.github.com/frfahim/ec020bc98fc5e52a74f1c12a82710546)
- [Linux Directory Structure](https://linuxhandbook.com/linux-directory-structure/)
- [Linux permission](https://linuxhint.com/linux_permissions_explained/#:~:text=Linux%20system%20has%20the%20permission%20parameter%20to%20distinguish,In%20addition%2C%20permission%20helps%20in%20defining%20user%20behavior.)
    + [File permission](https://www.guru99.com/file-permissions.html)

## 2. Exercise

### a. Research

- Learn about basic linux command, includes:
  - apt
  - sudo
  - ls, cd, mkdir, touch,...
  - tail, head,..
  - chmod
  - `>`, `>>` command
  - symlink (`ln` command)
  - ...
- Learn about concept: Permission, run command from file.
- Install other terminals than the default terminal.
- Install other shell than the default shell.
- Neovim.
- File permission.

### b. Practice

   **Environment**

1. Install other terminal/shell than the default.
2. Do some job with shell:
    1. install **neofetch**.
    2. install **neovim**.
    3. create new directory name: `homework`.
    4. create new file name `system_information` in `homework` directory.
    5. Write down the `neofetch` information into `system_information`.
    6. create directory name `system` in `homework` directory.
    7. move file `system_information` into `system` folder.
    8. duplicate `system` folder.
    9. set new folder name `archived`.
    10. create new directory name: `script` into `system`.
    11. create new file name `echo`
    12. write some `echo` command into `echo` file with **neovim**.
    13. set permission `execute` for `echo` file.
    14. run `echo` file.
    15. create new folder name `command_meaning` in `system` folder.
    16. writedown the user manual of any command (`man` command) you learn into file. (Ex: The `ls` manual command will be written to the file name `ls`).
        **Automatic this step will get big bonus point**
    17. Move the `command_meaning` folder to `archived` folder.

#### Advanced homework:
- Write a bash file to install all necessary tools, includes:
    + Update latest package via "Package manager"(apt)
    + Terminal (yakuake/tmux)
    + Shell (Zsh/Fish)
    + Git
    + Text editor
    + Browser
    + Zulip
    + Ibus-bamboo
    + ...

    *Use parallel is a bonus point*
