from timeit import default_timer
import requests


def timer(func):
    def wrapper(*args):
        start = default_timer()
        func(*args)
        end = default_timer()
        print('Returned from', func.__name__, '. It took', end-start, 'seconds')
    return wrapper 

@timer
def image_downloader(url='https://picsum.photos/200/300'):
    print('Start download image from link')
    img_data = requests.get(url).content
    with open('img.jpg', 'wb') as f:
        f.write(img_data)
    print('Completed!')

image_downloader()
