#DATABASE
## 1. Database object
Database object is a container containing all the schemas, records, logs, and constraints of the table. Databases are separated, which means that a user cannot access two databases together. \
To create database:
```
CREATE DATABASE db_name;
```
To drop a database:
```
DROP DATABASE [IF EXISTS] db_name;
```
## 2. Schema object
Schema object defines the outline of how the data is logically structured and stored in the database. It contains all the tables, data types, indexes, functions, stored procedures,...  Schema is like the folder in the database used to group logical objects together. One can define the different Schema in a database for different people accessing the application in order to avoid conflicts an unnecessary interference. \
A database can contain one or multiple schemas and each schema belongs to only one database. Two schemas can have different objects that share the same name. \
Schemas allow you to organize database objects into logical groups to make them more manageable and enable multiple users to use one database without interfering with each other. \
- Tables: the basic unit of data storage. Data is stored in rows and columns. You define a table with a table name and set of columns. You give each column a column name, a datatype and a width.
- Views: a view is a tailored presentation of tha data contained in one or more tables or other views. A view takes the output of  a query and treats it as a table. A view can be thougt of as a stored query or a virtual table
- Materialized views: used to summarize, compute, replicate and distribute data
- Indexes: the index helps you locate information faster
To create schema:
```
CREATE SCHEMA schema_name;
```
To drop a schema:
```
DROP SCHEMA [IF EXISTS] schema_name [, . . .] [CASCADE|RESTRICT];
```
`CASCADE` : it will automatically drop all the objects of the schema like functions, tables,... \
`RESTRICT` : it will refuse to drop the schema if it contains any objects like tables, fuctions... (default)
## 3. Data types
### 1. Numeric types
- Interger types: smallint, integer, bigint
- Arbitrary precision numbers: decimal, numeric
- Floating-Point types: real, double precision
- Serial Types: smallserial, serial, bigserial
### 2. Character types
- Variable-length with limit: charater varying(n), varchar(n)
- Fixed-length, blank padded: character(n), char(n)
- Variable unlimited length: text
### 3. Date/Time types
- timestamp
- date
- time
- interval
### 4. Boolean types
- boolean:
  +  'true' state: true, yes, on, 1
  +  'false' state: false, no, off, 0
### 5. Enumerated Types
Enumerated (enum) types are data types that comprise a static, ordered set of values. \
Example:
- Create an enum type:
```
CREATE TYPE pet('cat', 'dog')
```
- Remove an enum type:
```
DROP TYPE pet;
```
- Updating/Renaming a value:
```
#Add the new value to the existing type
ALTER TYPE pet ADD VALUE 'pig'; 
#Rename the existing type
ALTER TYPE pet RENAME TO pet_list;
```
*Note: enum labels are case sensitive and white space in the labels is significant.*
### 6. JSON types
JSON data types are for storing JSON data. JSON data types have the advantage of enforcing that each stored value is valid according to the JSON rules. There are also assorted JSON-specific functions and operators available for data stored in these data types
- json
- jsonb
  
The json data type stores an exact copy of the input text, which processing functions must reparse on each execution; while jsonb data is stored in a decomposed binary format that makes it slightly slower to input due to added conversion overhead, but significantly faster to process, since no reparsing is needed. jsonb also supports indexing, which can be a significant advantage.
## 4. Database normalization
Normaliztion is the process of minimizing redundancy from a ralation or set of relations. Redundancy in relation may cause insertion, deletion, and update anomalies. 
### 1NF Rules
- Each table cell should contain a single value
- Each record needs to be unique
### 2NF Rules
- Be in 1NF
- Single Column Primary Key that does not functionally dependant on any subset of candicate key relation
### 3NF Rules
- Be in 2NF
- Has no transitive funtional dependencies ssss
